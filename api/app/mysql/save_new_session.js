var mysqlQuery = require('../../mysql/query.js');

module.exports = function saveNewSession(payload) {
  var token = Math.ceil(Math.random() * 10000000).toString();
  return mysqlQuery({
    sql: 'INSERT INTO sessions (userId, token) VALUES (?, ?)',
    values: [payload.user.id, token]
  })
    .then(function (result) {
      payload.session = {
        id: result.rows.insertId,
        token: token
      };
      return payload;
    });
};
