var mysqlQuery = require('../../mysql/query.js');

function findUserByEmail(payload) {
  return mysqlQuery({
    sql: 'SELECT * FROM users WHERE email = ?',
    values: [payload.user.email]
  })
    .then(function (result) {
      if (result.rows[0])
        payload.user.id = result.rows[0].id;
      return payload;
    });
}

function createUserByEmail(payload) {
  return mysqlQuery({
    sql: 'INSERT INTO users (email) VALUES (?)',
    values: payload.user.email
  })
    .then(function (result) {
      payload.user.id = result.rows.insertId;
      return payload;
    });
}

module.exports = function findOrCreateUserByEmail(payload) {
  return findUserByEmail(payload)
    .then(function () {
      if (!payload.user.id)
        return createUserByEmail(payload);
    })
    .then(function () {
      return payload;
    });
};
