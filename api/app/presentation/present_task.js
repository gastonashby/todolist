module.exports = function presentTask(payload) {
  return Promise.resolve({
    task: {
      id: payload.task.id,
      text: payload.task.text
    }
  });
};
