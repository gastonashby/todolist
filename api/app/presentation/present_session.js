module.exports = function presentSession(payload) {
  return Promise.resolve({
    session: { token: payload.session.token }
  });
};
