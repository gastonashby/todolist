module.exports = function validateNewTask(payload) {
  var errors = {};

  if (!payload.newTask)
    errors['newTask'] = 'MISSING';
  else if (!payload.newTask.text)
    errors['newTask.text'] = 'MISSING';

  if (Object.keys(errors).length != 0)
    return Promise.reject(errors);

  return Promise.resolve(payload);
};
