var mysqlQuery = require('../../mysql/query.js')

module.exports = function emptyDatabase() {
  return Promise.all([
    mysqlQuery({ sql: 'DELETE FROM users' }),
    mysqlQuery({ sql: 'DELETE FROM sessions' }),
    mysqlQuery({ sql: 'DELETE FROM tasks' }),
  ]);
};
