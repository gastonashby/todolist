describe('presentSession', function () {
  var fn = require('../../app/presentation/present_session.js');

  it('presents session', function (done) {
    var payload = {
      user: {
        id: Math.random(),
        email: Math.random()
      },
      session: {
        id: Math.random(),
        token: Math.random()
      }
    };
    fn(payload).then(function (result) {
      expect(result).toEqual({ session: { token: payload.session.token } });
      done();
    });
  });
});
