var API_URL = 'https://api.todolist.init.uy/';

function makeApiRequest(config) {
  return new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.addEventListener('load', function () {
      var body = JSON.parse(xhr.responseText);
      if (xhr.status != 200) return reject(body);
      else return resolve(body);
    });
    xhr.open('POST', API_URL + config.api);
    xhr.send(JSON.stringify(config.body));
  });
}
